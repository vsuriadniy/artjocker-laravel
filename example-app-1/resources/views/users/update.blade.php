@extends('layouts.app')

@section('content')
   <div style="margin-left: 5px">
       <h1>Update user <b>{{ $user->name }}</b></h1>
       <form method="POST" action="{{ route('users.update', $user) }}" enctype="multipart/form-data">
           @method('PUT')
           @csrf
           <div >
               <input type="hidden" name="is_block" value="0" {{ $user->is_block ? "checked" : '' }}>
               <input type="checkbox" name="is_block" value="1" {{ $user->is_block ? "checked" : '' }}>
               <label>Block user</label>
           </div>

           <div class="row mt-3">
               <div class="col-sm-4">
                   <input name="name" type="text" class="form-control @error('name') alert alert-danger @enderror"
                          placeholder="Name" aria-label="Name" value="{{ $user->name }}">
                   @error('name')
                   <div class="form-text" style=" color: red">{{ $message }}</div>
                   @enderror
               </div>
           </div>

           <div class="row mt-3 ">
               <div class="col-sm-4">
                   <input name="email" type="text" class="form-control @error('email') alert alert-danger @enderror"
                          placeholder="Email" aria-label="Email" value="{{ $user->email }}">
                   @error('email')
                   <div class="form-text" style=" color: red">{{ $message }}</div>
                   @enderror
               </div>
           </div>

           <div class="row mt-3">
               <div class="col-sm-4">
                   <tr>
                       @if($user->getFirstMediaUrl('avatar', 'thumb'))
                           <td><img src="{{$user->getFirstMediaUrl('avatar', 'thumb') }}" width="90px" height="90px"></td>
                       @endif
                   </tr>
               </div>
               <div class="row mt-3">
                   <input type="file" name="avatar">
               </div>
           </div>

           <div class="row mt-3">
               <div class="col-sm-4">
                   <input class="btn btn-warning" type="submit" value="Save">
                   <a type="button" class="btn btn-dark" href="{{ route('users.index') }}">Cancel</a>
               </div>
           </div>
       </form>
   </div>
@endsection
