@extends('layouts.app')

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-danger">{{ Session::get('message') }}</div>
    @endif
    <table class="table">
        <thead>
        <tr>
            <th>Photo</th>
            <th>Name</th>
            <th>Email</th>
            <th>Is_block</th>
            <th>Actions</th>
            <th>Roles</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                @if($user->getFirstMediaUrl('avatar', 'thumb'))
                    <td><img src="{{$user->getFirstMediaUrl('avatar', 'thumb') }}" width="90px" height="90px"></td>
                @else
                    <td>No photo</td>
                @endif
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->is_block ? 'Blocked' : 'Not blocked'}}</td>
                <td>
                    <form method="post" action="{{ route('users.destroy', $user) }}">
                        @csrf
                        <a type="button" class="btn btn-info" href="{{ route('users.show', $user) }}">Show</a>
                            <a type="button" class="btn btn-warning" href="{{ route('users.edit', $user) }}">Update</a>
                        @method('DELETE')
                            <input class="btn btn-danger" type="submit" value="Delete">
                    </form>
                </td>
                <td>
                    @foreach ($user->roles as $role)
                        @if($role->name)
                            <span class="badge bg-primary">{{ $role->name }}</span>
                        @endif
                    @endforeach
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
