@extends('layouts.app')

@section('content')
    <h1>User <b>{{ $user->name }}</b></h1>
    <div class="col-md-12">
        <table class="table">
            <tbody>
            <tr>
                <th>
                    Field
                </th>
                <th>
                    Value
                </th>
            </tr>
            <tr>
                <td>ID</td>
                <td>{{ $user->id }}</td>
            </tr>
            <tr>
                <td>Photo</td>
                @if($user->getFirstMediaUrl('avatar', 'thumb'))
                    <td><img src="{{$user->getFirstMediaUrl('avatar', 'thumb') }}" width="90px" height="90px"></td>
                @else
                    <td>No photo</td>
                @endif
            </tr>
            <tr>
                <td>Name</td>
                <td>{{ $user->name }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <td>Is_block</td>
                <td>{{ $user->is_block }}</td>
            </tr>
            <tr>
                <td>Google_id</td>
                <td>{{ $user->google_id }}</td>
            </tr>
            <tr>
                <td>Created_at</td>
                <td>{{ $user->created_at }}</td>
            </tr>
            <tr>
                <td>Updated_at</td>
                <td>{{ $user->updated_at }}</td>
            </tr>
            </tbody>
        </table>
        <div>
            <a type="button" class="btn btn-dark" href="{{ route('users.index') }}">Back</a>
        </div>
    </div>
@endsection
