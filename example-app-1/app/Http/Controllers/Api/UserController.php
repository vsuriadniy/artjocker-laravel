<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Models\User;
use Illuminate\Http\Request;
use function response;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::select('name', 'email')->whereNotNull('google_id')->where('is_block', '=', 0)->get();
        return response()->json(['success' => true, 'users' => new UserCollection($users)]);
    }
}
