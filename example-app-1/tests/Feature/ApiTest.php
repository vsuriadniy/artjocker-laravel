<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_checkStatus()
    {
        $response = $this->get('/api/oauth-users?is_from_third_party_service=true');
        $response->assertStatus(200);
    }
}
