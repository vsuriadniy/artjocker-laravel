<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GoogleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_checkAuthStatus()
    {
        $response = $this->get('/auth/google');
        $response->assertStatus(302);
    }

    public function test_checkCallbackStatus()
    {
        $response = $this->get('/auth/google/callback');
        $response->assertStatus(302);
    }
}
