<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles_has_users')->insert([
            'role_id' => Role::firstWhere('name', 'admin')->id,
            'user_id' => User::first()->id,
        ]);
    }
}
